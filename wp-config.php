<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'bdes');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J[P~~Uu9tgR%WX-5L^G:f4-g M1uBPc@vi8)yaDEOmV9Qtuf>[[?mXt.#O0~D,?_');
define('SECURE_AUTH_KEY',  'KO=57*Qa(F22?RyqC=X42fb%C {1<hfji C|5:095UW|VfIDL]&g>xX>d9I`5SmP');
define('LOGGED_IN_KEY',    'v|9OEA2an@+X#Bo)QOVj:[vNmI0zrR@ikzD~j)%$yIs4u%&%yB9z5$re<p?eWC*P');
define('NONCE_KEY',        'I~Gm,q//^Ohk/@VawTqOn-PJcz=.sV]U{nRq2zXT-WdCiegxuH/{HmYa}iB=6krA');
define('AUTH_SALT',        ' LUlU0%pc6o`yENwa&I[K22OSLJU_9P|_f`wv-gIC8G]U$RcoDvrCe($|wVMporT');
define('SECURE_AUTH_SALT', '&]E_K,rtP}m18x0?v7YOS=<Yno.n,d<^C 9,vb dp=D7v7!j=DhjP1Z[#[u#bgH$');
define('LOGGED_IN_SALT',   '(Q=j;;TytAv $@4S%c6H)2I@*@>u{H}&!DeFoG6u}Y]CL6 w( [`cYKD-qh[usv)');
define('NONCE_SALT',       'g_M#XU?6q Gj!9jl]h$2LVaMGxeDa{Ux[q(#T2 [6G:D/oQGT D,|<^ Mz<S]fWT');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');