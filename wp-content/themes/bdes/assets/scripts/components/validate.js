;(function validate($){

    $(window).ready(init);

    var $VALIDATE = '.Validate',
        $CLOSE_VALIDATE = '.Validate-redirect';

    function init() {

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        var validateCookie = getCookie("validate"),
            validateBox = $($VALIDATE);

        if (validateCookie === "true") {
            validateBox.hide();
        }

        var close = $($CLOSE_VALIDATE);
        close.on('click', hideConditions);
    }

    function hideConditions() {
        var validate = $($VALIDATE);
        validate.hide();
        document.cookie = "validate=true";
    }

})(jQuery);