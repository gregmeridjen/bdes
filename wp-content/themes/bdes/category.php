<?php
/**
 * The template for displaying Category pages.
 *
 */

$cpt = get_queried_object()->name;

$context = Timber::get_context();

$toto = Timber::get_term($cpt);

$context['posts'] = Timber::get_term($cpt);
$context['test'] = var_dump($toto);

Timber::render( 'category.twig', $context );