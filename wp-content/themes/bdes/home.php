<?php
/**
 * The template for displaying front page
 * Template name: Homepage
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( array( 'frontpage.twig' ), $context );